﻿'use strict';
const commander = require('commander');
const puppeteer = require('puppeteer');
const path = process.cwd();

commander
    .version('1.0.0', '-v, --version')
    .usage('[OPTIONS]...')
    .option('-i, --input <file-path>', 'The path of the HTML file', 'output/index.html')
    .option('-o, --output <file-path>', 'The path of the PDf file', 'output/files/resume.pdf')
    .parse(process.argv);

const htmlFile = commander.input;
const destinationFile = commander.output;

(async() => {

    var options = {
        format: 'A4',
        path: destinationFile,
        printBackground: false,
        margin: {
            top: "0px",
            bottom: "0px",
            left: "0px",
            right: "0px"
        },
    }
    const browser = await puppeteer.launch({
        headless: true,
        args: ['--no-sandbox', '--disable-setuid-sandbox']
    });
    const page = await browser.newPage();
    await page.goto(`file://${path}/${htmlFile}`, { waitUntil: 'networkidle2' });
    await page.pdf(options);
    await browser.close();
})();