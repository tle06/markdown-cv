﻿title: Resume
name: John
lastname: Doe
jobtitle: System administrator
picture: template.png
icons:
  - name: Email
    value: john.doe@email.fr
    icon: fas fa-envelope
  - name: Telephone
    value: +35 659 97 87 16
    icon: fas fa-mobile-alt
  - name: Skype
    value: john.doe.pro
    icon: fab fa-skype
  - name: linkedin
    value: johndoe
    icon: fab fa-linkedin
languages:
  - name: English
    level: Native
  - name: Spanish
    level: Fluent
  - name: German
    level: A2

---

# Experience

### 11.2006-03.2010 [3Y]

### Company 4

#### Job title 4 in Country 4

Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit

- __My job was to__
  - Task
  - Task
  - Task
  - Task
  - Task
  - Task
  - Task
  - Task
  - Task
  - Task
  - Task
- __The scope covered__
  - Scope
  - Scope
  - Scope
  - Scope
  - Scope
- __Main projects__
  - Project
  - Project
  - Project
  - Project
  - Project
  - Project
  - Project
  - Project
  - Project

### 10.2005-11.2006 [1Y]

### Company 3

#### Job title 3 in Country 3

- Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud
  - Task
  - Task
  - Task
  - Task
  - Task

### 10.2004-10.2005 [1Y]

### Company 2

#### Job title 2 in Country 2

- Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.

### 09.2001-09.2004 [3Y]

### Company 1

#### Job title 1 in Country 1

- Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.

---

# Education

### 2001-2004

School 4 - Diploma 4 - Country

### 2003-2004

School 3 - Diploma 3 - Country

### 2000-2001

School 2 - Diploma 2 - Country

### 2000-2000

School 1 - Diploma 1 - Country

---

# Skills

### Main

- Skill
- Skill
- Skill
- Skill
- Skill
- Skill
- Skill
- Skill
- Skill
- Skill
- Skill

### Technologies

- Skill
- Skill
- Skill
- Skill
- Skill
- Skill
- Skill
- Skill
- Skill
- Skill
- Skill
- Skill
- Skill
- Skill
- Skill
- Skill
- Skill
- Skill
