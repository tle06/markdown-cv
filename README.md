# Generate markdown CV to HTML and PDF

## Description

Write your CV/resume in markdown then transform it into a static website on S3 or gitlab page and make a printable PDF of it.

## Demo

* [html](https://cv.demo.tlnk.fr)
* [pdf](https://cv.demo.tlnk.fr/files/resume.pdf)

## Tools used

* [Markdown-styles](https://github.com/mixu/markdown-styles)
* [Handlebars](https://handlebarsjs.com/)
* [Puppeteer](https://github.com/puppeteer/puppeteer)
* [Commander](https://github.com/tj/commander.js)
* [Nord theme color](https://www.nordtheme.com/)
* nodejs
* AWS S3
* AWS Certificate
* AWS Cloudfront
* Gitlab CI/CD
* Gitlab Pages

## Content

The sources files are presented as below

```txt
.
├── input
│   └── index.md
└── layout
    ├── assets
    │   ├── css
    │   │   ├── layout.css
    │   │   └── paper.css
    │   └── img
    │       └── picture.png
    └── page.html
```

* ```index.md``` contain your CV/Resume write in markdown
* ```page.html``` contain the HTML template where the markdown will be injected
* ```picture.png``` is your picture
* ```layout.css``` contains the template CSS
* ```paper.css``` is used for printing properties

### output

The output files are presented as below

```txt
.
├── assets
│   ├── css
│   │   ├── layout.css
│   │   └── paper.css
│   └── img
│       └── picture.png
├── files
│   └── resume.pdf
└── index.html
```

* ```index.html``` will contain your markdwon transformed into HTML
* ```resume.pdf``` it's the pdf file of ```index.html``` (style included)

The output is then uploaded to S3 bucket root folder. You can consult your HTML page under ```https://cv.demo.tlnk.fr``` and you can download the pdf version ```https://cv.demo.tlnk.fr/files/resume.pdf```

## Setup

### Gitlab config

1. Clone the repo
2. Add the following environment variable in GitLab settings

* AWS_ACCESS_KEY_ID
* AWS_DEFAULT_REGION
* AWS_SECRET_ACCESS_KEY
* S3_BUCKET_NAME = name of your S3 bucket

3. Add runner into your project and update the tag on the ```.gitlab-ci.yml``` file

```yml
job-generate-files:
  tags:
    - stage
    - docker
.
.
.

job-upload:
  tags:
    - stage
    - docker
```

### AWS config

1. Create an S3 bucket ready for the static website by following this [tutorial](https://medium.com/@channaly/how-to-host-static-website-with-https-using-amazon-s3-251434490c59). All the step to create a static website with https are explained.

2. Create a user in IAM, for example, ```cv.demo.tlnk.fr```
3. Create a group in IAM, for example, ```CvDemoS3BackendRole```
4. Add the user into the group
5. Create a policy in IAM, for example, ```CvDemoBackendS3Policy```
6. Link the policy to the group
7. Add the following policy to the role, don't forget to __update the s3 bucket name under Ressource__

```json
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "VisualEditor0",
            "Effect": "Allow",
            "Action": [
                "s3:PutObject",
                "s3:ListBucket",
                "s3:DeleteObject"
            ],
            "Resource": [
                "arn:aws:s3:::cv.demo.tlnk.fr",
                "arn:aws:s3:::cv.demo.tlnk.fr/*"
            ]
        }
    ]
}
```

8. Update ```index.md``` under the ``src`` folder
9. Commit and merge your file into master
10. Trigger the gitlab CI/CD pipeline

### Gitlab Pages

The third job will upload the files into a folder named Public then publish it as described in the [documentation](https://gitlab.com/help/user/project/pages/index.md).

Your website will be available under **https://username.gitlab.io/projectName**. Exemple: [https://tle06.gitlab.io/markdown-cv](https://tle06.gitlab.io/markdown-cv).

You can also add a custom domain under the project **settings > Pages**. There you will have to enter a domain or subdomain then register few DNS records (CNAME and TXT) to verify your ownership. Exemple: [https://resume.demo.tlnk.fr](https://resume.demo.tlnk.fr).